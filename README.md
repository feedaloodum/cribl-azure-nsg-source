# Azure NSG Flow Log Source Pack
----

This Cribl Source Pack for Azure NSG Flow Logs maps them into the Cribl Common Schema.  This pack is designed to be applied to the source where the Azure NSG Flow Logs are coming in.


## Installation

1. Install this pack from the Cribl Pack Dispensary, use the Git clone feature inside Cribl Stream, or download the most recent .crbl file from the repo
2. Apply the Pack to the Source where the Azure NSG Flow logs are coming in.


## Release Notes

### Version 1.0.0 - 2023-12-14
Initial Release supporting Azure NSG Flow logs

## Contributing to the Pack
Discuss this pack on our Community Slack channel #packs.

## Contact
The author of this pack is Dan Schmitz and can be contacted at <dschmitz@cribl.io>.